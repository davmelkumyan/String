﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Temperature
{
    class Program
    {
        static void Main(string[] args)
        {
            var t = new Temperature(30);
            Console.WriteLine("Temperature is {0:C}", t);
            Console.WriteLine("Temperature is {0:F}", t);
            Console.WriteLine("Temperature is {0:K}", t);
            Console.WriteLine("Temperature is {0}", t);
        }

        class Temperature : IFormattable
        {
            public Temperature(decimal temperature)
            {
                if (temperature < -273.15m)
                {
                    throw new ArgumentOutOfRangeException("Temperature is less then Absolute Zero");
                }
                Celsius = temperature;
            }

            public decimal Celsius { get; }

            public decimal Faranheit
            {
                get { return this.Celsius * 9 / 5 + 32; }
            }

            public decimal Kelvin
            {
                get { return this.Celsius + 273.15m; }
            }

            //переопределение метода из object

            public override string ToString()
            {
                return ToString("G", CultureInfo.CurrentCulture);
            }

            //реализация интерфейса
            public string ToString(string format, IFormatProvider provider)
            {
                if (string.IsNullOrEmpty(format))
                    format = "G";

                if (provider == null)
                    provider = CultureInfo.CurrentCulture;

                switch (format.ToUpperInvariant())
                {
                    case "G":
                    case "C":
                        return Celsius.ToString( "f2", provider) + " C";
                    case "F":
                        return Faranheit.ToString("F2", provider) + " F";
                    case "K":
                        return Kelvin.ToString("F2", provider) + "K";
                    default:
                        throw new FormatException($"The {format} format string is not");
                }
            }
        }
    }
}
