﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String.Work
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = "c:\\windows\\system32";
            Console.WriteLine(s1);
            string s2 = @"c:\windows\system32";
            Console.WriteLine(s2);
            if (ReferenceEquals(s1, s2))
            {

            }
            //string s3 = Console.ReadLine(); //Не проверяет есть ли такая строка в таблице интернирования
            string s4 = string.Intern(Console.ReadLine());//Провеяет в таблице
            if (ReferenceEquals(s1, s4))
            {

            }
        }
    }
}
