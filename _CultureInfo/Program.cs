﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace _CultureInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            CultureInfo cultureInfo = CultureInfo.CurrentCulture;
            Console.WriteLine("The current locale is {0}", cultureInfo);

            CultureInfo[] cultureArray = CultureInfo.GetCultures(CultureTypes.AllCultures);
            Console.WriteLine(cultureArray.Length);

            foreach (var item in cultureArray)
            {
                Console.WriteLine(item);
            }

            cultureArray = CultureInfo.GetCultures(CultureTypes.UserCustomCulture);
            if(cultureArray.Length < 1)
            {
                Console.WriteLine("NO User Cultures");
            }
        }
    }
}
