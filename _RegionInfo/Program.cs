﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace _RegionInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            RegionInfo region = RegionInfo.CurrentRegion;
            Console.WriteLine(region.EnglishName);
            Console.WriteLine(region.NativeName);
            Console.WriteLine(region.CurrencyNativeName);
            Console.WriteLine(region.CurrencyEnglishName);
            Console.WriteLine(region.CurrencySymbol);

            Console.WriteLine(new string('-', 30));

            string[] days = CultureInfo.CurrentCulture.DateTimeFormat.DayNames;
            foreach (var item in days)
            {
                Console.WriteLine(item);
            }

            //Культура Германии
            string[] dayNames = CultureInfo.GetCultureInfo("de-DE").DateTimeFormat.DayNames;
            foreach (var item in dayNames)
            {
                Console.WriteLine(item);
            }
        }
    }
}
