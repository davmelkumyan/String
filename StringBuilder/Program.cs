﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace _StringBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            var america = CultureInfo.GetCultureInfo("en-US");
            var russia = CultureInfo.CurrentCulture;
            var complex = new Complex(12.54, 87.65);
            Console.WriteLine(complex.ToString("COMP", america));
            Console.WriteLine(complex.ToString("COMP", russia));
        }

        struct Complex : IFormattable
        {
            public double Real { get;}
            public double Imagine { get;}

            public Complex(double real, double imagine)
            {
                Real = real;
                Imagine = imagine;
            }

            public override string ToString()
            {
                return this.ToString("COMP", CultureInfo.CurrentCulture);
            }

            public string ToString(string format, IFormatProvider provider)
            {
                var builder = new StringBuilder();
                if (format == "COMP")
                {
                    builder.Append(GetType() + "\n");
                    builder.AppendFormat("Real number:\t{0} \n", Real.ToString(provider));
                    builder.AppendFormat("Imagine number:\t{0} \n", Imagine.ToString(provider));
                }
                else
                {
                    builder.Append(" ( ");
                    builder.Append(Real.ToString(format, provider));
                    builder.Append(" : ");
                    builder.Append(Imagine.ToString(format, provider));
                    builder.Append(" ) ");
                }
                return builder.ToString();
            }

        }
    }
}
