﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Forms;

namespace ExampleForMoney
{
    class Program
    {
        static void Main(string[] args)
        {
            decimal money = 25400;
            var cultureInfo1 = new CultureInfo("en-US");
            var cultureInfo2 = new CultureInfo("de-DE");
            var cultureInfo3 = new CultureInfo("ru-RU");

            string moneyInfo = money.ToString("C", cultureInfo1);
            string result = string.Format("Money of US - {0} ", moneyInfo);

            moneyInfo = money.ToString("C", cultureInfo2);
            result += string.Format("\nMoney of Germany - {0} ", moneyInfo);

            moneyInfo = money.ToString("C", cultureInfo3);
            result += string.Format("\nMoney of Russia - {0} ", moneyInfo);
            MessageBox.Show(result);
        }
    }
}
